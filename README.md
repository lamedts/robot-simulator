# Toy Robot Simulator

```bash
# to install module
$ npm install 
# to run
$ npm start
$ Toy-Robot-Simulator~$ PLACE 1,2,EAST
$ Toy-Robot-Simulator~$ RIGHT
$ Toy-Robot-Simulator~$ MOVE
$ Toy-Robot-Simulator~$ REPORT
$ Toy-Robot-Simulator~$ 1,1,SOUTH
```

```

  Commands:

    help [command...]   Provides help for a given command.
    exit                Exits application.
    PLACE <x,y,facing>  Place on x,y with <facing>
    MOVE                Move one step
    LEFT                Turn left
    RIGHT               Turn RIGHT
    REPORT              Report position

```