import { expect } from 'chai';
import Robot from '../src/lib/Robot.js';

describe('Test Robot Class', () => {
  let robot;
  before(function() {
    robot = new Robot();
    robot.place(4, 4, 'NORTH');
  });
  describe('test `place`', () => {
    it('should at 4, 4 & facing north', () => {
      robot.place(4, 4, 'NORTH');
      expect(robot.currentPos.x).equal(4);
      expect(robot.currentPos.y).equal(4);
      expect(robot.getFacing()).equal('NORTH');
    });
  });
  describe('test `move`', () => {
    it('move #1', () => {
      robot.move();
      expect(robot.currentPos.x).equal(4);
      expect(robot.currentPos.y).equal(5);
      expect(robot.getFacing()).equal('NORTH');
    });
    it('test `move` #2', () => {
      robot.move();
      expect(robot.currentPos.x).equal(4);
      expect(robot.currentPos.y).equal(6);
      expect(robot.getFacing()).equal('NORTH');
    });
  });
  describe('test `rotate`', () => {
    it('test `rotate` #1', () => {
      robot.left();
      expect(robot.getFacing()).equal('WEST');
    });
    it('test `rotate` #2', () => {
      robot.left();
      expect(robot.getFacing()).equal('SOUTH');
    });
    it('test `rotate` #3', () => {
      robot.left();
      expect(robot.getFacing()).equal('EAST');
    });
    it('test `rotate` #4', () => {
      robot.left();
      expect(robot.getFacing()).equal('NORTH');
    });
    it('test `rotate` #5', () => {
      robot.right();
      expect(robot.getFacing()).equal('EAST');
    });
  });
});
