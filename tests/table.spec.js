import { expect } from 'chai';
import Table from '../src/lib/Table.js';

let table;

describe('Test Table Class', () => {
  describe('default widht & height', () => {
    it('Create Table w/ default width & height ', () => {
      table = new Table();
      expect(table).to.be.a('object');
    });

    it('Width of Table = 5', () => {
      expect(table.width).equal(5);
    });

    it('Height of Table = 5', () => {
      expect(table.height).equal(5);
    });

    it('return false when location is (1, 4)', () => {
      expect(table.isOnTop(1, 4)).equal(true);
    });

    it('return true when location is (7, 8)', () => {
      expect(table.isOnTop(7, 8), false);
    });
  });
  describe('custom widht & height', () => {
    it('Create Table w/ width 6 & height 8', () => {
      table = new Table(6, 8);
      expect(table).to.be.a('object');
    });

    it('Width of Table = 6', () => {
      expect(table.width).equal(6);
    });

    it('Height of Table = 8', () => {
      expect(table.height).equal(8);
    });

    it('return false when location is (1, 4)', () => {
      expect(table.isOnTop(1, 4)).equal(true);
    });

    it('return true when location is (7, 8)', () => {
      expect(table.isOnTop(7, 8), false);
    });
  });
});
