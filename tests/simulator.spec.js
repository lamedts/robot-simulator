import { expect } from 'chai';
import Simulator from '../src/lib/Simulator';
import Table from '../src/lib/Table';
import Robot from '../src/lib/Robot';
import * as Types from '../src/lib/Types';

describe('Test Simulator', () => {
  let simulator = new Simulator(new Robot(), new Table());
  describe('test `report`', () => {
    it('report return', () => {
      try {
        simulator.report();
      } catch (err) {
        expect(err.message).to.equal('Not place yet');
      }
    });
  });
  describe('test `action`', () => {
    it('PLACE 1,2,EAST', () => {
      simulator.action('PLACE', {x: 1, y: 2, facing: 'EAST'});
      expect(simulator.report()).to.eql({x: 1, y: 2, facing: Types.Facing.EAST});
    });
    it('RIGHT', () => {
      simulator.action('RIGHT');
      expect(simulator.report()).to.eql({x: 1, y: 2, facing: Types.Facing.SOUTH});
    });
    it('MOVE', () => {
      simulator.action('MOVE');
      expect(simulator.report()).to.eql({x: 1, y: 1, facing: Types.Facing.SOUTH});
    });
  });
});
