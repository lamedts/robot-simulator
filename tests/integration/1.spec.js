import { expect } from 'chai';
import Simulator from '../../src/lib/Simulator';
import Table from '../../src/lib/Table';
import Robot from '../../src/lib/Robot';
import * as Types from '../../src/lib/Types';

describe('integration', () => {
  let simulator = new Simulator(new Robot(), new Table());
  describe('case 1', () => {
    it('move 1', () => {
      simulator.action('PLACE', {x: 1, y: 2, facing: 'EAST'});
      simulator.action('RIGHT');
      simulator.action('MOVE');
      expect(simulator.report()).to.eql({x: 1, y: 1, facing: Types.Facing.SOUTH});
    });
    it('move 2', () => {
      try {
        simulator.action('MOVE');
        simulator.action('MOVE');
        simulator.action('MOVE');
        simulator.action('MOVE');
        simulator.report();
      } catch (err) {
        expect(err.message).to.eql('Will Fall, ignore cmd');
      }
    });
    it('move 3', () => {
      simulator.action('RIGHT');
      simulator.action('RIGHT');
      simulator.action('MOVE');
      expect(simulator.report()).to.eql({x: 1, y: 0, facing: Types.Facing.NORTH});
    });
  });
});
