import { expect } from 'chai';
import * as Types from '../src/lib/Types';

describe('Test Types', () => {
  describe('test `FacingToDegree`', () => {
    it('north return 0', () => {
      expect(Types.FacingToDegree('north')).to.equal(0);
    });
    it('NORTH return 0', () => {
      expect(Types.FacingToDegree('north')).to.equal(0);
    });
    it('abc return null', () => {
      expect(Types.FacingToDegree('abc')).to.equal(null);
    });
  });
  describe('test `DegressToFace`', () => {
    it('0 return NORTH', () => {
      expect(Types.DegreeToFacing(0)).to.equal('NORTH');
    });
    it('360 return null', () => {
      expect(Types.DegreeToFacing(360)).to.equal(null);
    });
  });
});
