const vorpal = require('vorpal')();

import Simulator from '../src/lib/Simulator';
import Table from '../src/lib/Table';
import Robot from '../src/lib/Robot';
import * as Types from '../src/lib/Types';

function parseIntNumber(v) {
  try {
    v = parseInt(v);
    if (isNaN(v)) throw new Error('Not Int');
    return v;
  } catch (err) {
    throw new Error('Not Int');
  }
}

const simulator = new Simulator(new Robot(), new Table());

vorpal.command('PLACE <x,y,facing>')
  .description('Place on x,y with <facing>')
  .action((args, callback) => {
    try {
      let params = args['x,y,facing'].split(',');
      if (params.length != 3) {
        throw new Error('Require parameter in x,y,facing format!');
      }
      let [x, y, facing] = params;
      x = parseIntNumber(x);
      y = parseIntNumber(y);
      facing = facing.toUpperCase();
      simulator.action(Types.CMD.PLACE, {x, y, facing});
    } catch (err) {
      process.stdout.write(`Error: ${err.message}\n`);
    }
    callback();
  });

vorpal.command('MOVE')
  .description('Move one step ')
  .action((args, callback) => {
    try {
      simulator.action(Types.CMD.MOVE);
    } catch (err) {
      process.stdout.write(`Error: ${err.message}\n`);
    }
    callback();
  });

vorpal.command('LEFT')
  .description('Turn left')
  .action((args, callback) => {
    try {
      simulator.action(Types.CMD.LEFT);
    } catch (err) {
      process.stdout.write(`Error: ${err.message}\n`);
    }
    callback();
  });

vorpal.command('RIGHT')
  .description('Turn RIGHT')
  .action((args, callback) => {
    try {
      simulator.action(Types.CMD.RIGHT);
    } catch (err) {
      process.stdout.write(`Error: ${err.message}\n`);
    }
    callback();
  });

vorpal.command('REPORT')
  .description('Report position')
  .action((args, callback) => {
    try {
      let position = simulator.report();
      process.stdout.write(`${position.x},${position.y},${position.facing}\n`);
    } catch (err) {
      process.stdout.write(`Error: ${err.message}\n`);
    }
    callback();
  });

vorpal.delimiter('Toy-Robot-Simulator~$').show();