import * as Types from './Types';

class Robot {
  constructor() {
    this.degree = null;
    this.nextPos = {
      x: null,
      y: null
    };
    this.currentPos = {
      x: null,
      y: null
    };
    this.placed = false;
  }

  setFacing(facing) {
    this.degree = Types.FacingToDegree(facing);
  }

  getFacing() {
    return Types.DegreeToFacing(this.degree);
  }

  setPos() {
    this.currentPos = Object.assign({}, this.nextPos);
  }

  place(x, y, facing) {
    this.currentPos.x = x;
    this.currentPos.y = y;
    this.setFacing(facing);
  }

  left() {
    this.degree = this.degree - 90 === -90 ? 270 : this.degree - 90;
  }

  right() {
    this.degree = this.degree + 90 === 360 ? 0 : this.degree + 90;
  }

  move() {
    this.nextPos = Object.assign({}, this.currentPos);
    switch (this.degree) {
      case 0: 
        ++this.nextPos.y;
        break;
      case 90:
        ++this.nextPos.x;
        break;
      case 180: 
        --this.nextPos.y;
        break;
      case 270: 
        --this.nextPos.x;
        break;
    }
    if (!this.placed) this.setPos();
  }
}

export default Robot;