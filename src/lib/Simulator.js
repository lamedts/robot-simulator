import * as Types from './Types';
class Simulator {
  constructor(toy, place) {
    this.toy = toy;
    this.toyOnTop = false;
    this.place = place;
  }

  // param action: all action for toy
  action(action, param) {
    switch (action.toUpperCase()) {
      case Types.CMD.PLACE:
        if (!this.place.isOnTop(param.x, param.y)) throw new Error('Not place on top');
        this.toy.place(param.x, param.y, param.facing);
        this.toyOnTop = true;
        break;
      case Types.CMD.MOVE:
        this.toy.move();
        if (!this.place.isOnTop(this.toy.nextPos.x, this.toy.nextPos.y)) {
          throw new Error('Will Fall, ignore cmd');
        } else {
          this.toy.setPos();
          this.toyOnTop = true;
        }
        break;
      case Types.CMD.LEFT: 
        this.toy.left();
        break;
      case Types.CMD.RIGHT: 
        this.toy.right();
        break;
    }
  }

  report() {
    if (!this.toyOnTop) throw new Error('Not place yet');
    return {
      x: this.toy.currentPos.x,
      y: this.toy.currentPos.y,
      facing: this.toy.getFacing(),
    };
  }
  
}

export default Simulator;