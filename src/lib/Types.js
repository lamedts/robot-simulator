export let CMD = {
  PLACE: 'PLACE',
  MOVE: 'MOVE',
  LEFT: 'LEFT',
  RIGHT: 'RIGHT',
  REPORT: 'REPORT',
};

export let Facing = {
  NORTH: 'NORTH',
  EAST: 'EAST',
  SOUTH: 'SOUTH',
  WEST: 'WEST',
};

export let FacingToDegree = function(facing) {
  switch(facing.toUpperCase()) {
    case Facing.NORTH:
      return 0;
    case Facing.EAST:
      return 90;
    case Facing.SOUTH:
      return 180;
    case Facing.WEST:
      return 270;
    default:
      return null;
  };
};

export let DegreeToFacing = function(degree) {
  switch(degree) {
    case 0:
      return Facing.NORTH;
    case 90:
      return Facing.EAST;
    case 180:
      return Facing.SOUTH;
    case 270:
      return Facing.WEST;
    default:
      return null;
  };
};
