class Table {
  // create Table with default param if not specific
  constructor(width = 5, height = 5) {
    this.width = width;
    this.height = height;
  }

  // check if the position of the object is on the table
  isOnTop(x, y) {
    if (x < 0 || y < 0) return false;
    if (x > (this.width - 1)) return false;
    if (y > (this.height - 1)) return false;
    return true;
  }
}

export default Table;